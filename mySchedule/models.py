from django.db import models

# Create your models here.

class Schedule(models.Model):
    nama = models.CharField(max_length = 30)
    hari = models.CharField(max_length = 7)
    tanggal = models.DateField()
    jam = models.TimeField()
    tempat = models.CharField(max_length = 10)
    kategori = models.CharField(max_length = 12)


    def __str__(self):
        return "{}.{}".format(self.id,self.nama)